// Function description

// Complete the diagonal difference  function in the editor below.

// diagonalDifference takes the following parameter:

// int arr[n][m]: an array of integers
// Return
// int: the absolute diagonal difference

'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the 'diagonalDifference' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts 2D_INTEGER_ARRAY arr as parameter.
 */

function diagonalDifference(arr) {
    // Write your code here
    // membuat variabel panjang array
    var n = arr.length;
    // membuat variabel yang menampung jumlah d1 dan d2
    var d1 = 0;
    var d2 = 0;
    // melakukan pengecekan data array i
    for (var i = 0; i < n; i++) {
        // melakukan pengecekan data array j
        for (var j = 0; j < n; j++) {
            // mencari jumlah dari diagonal pertama
            if (i === j) {
                d1 += arr[i][j];
            }
            // mencari jumlah dari diagonal kedua
            if (i + j === n - 1) {
                d2 += arr[i][j];
            }
        }
    }
    // mengembalikan hasil pengurangan d1 - d2
    return Math.abs(d1 - d2);
}



function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const n = parseInt(readLine().trim(), 10);

    let arr = Array(n);

    for (let i = 0; i < n; i++) {
        arr[i] = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));
    }

    const result = diagonalDifference(arr);

    ws.write(result + '\n');

    ws.end();
}